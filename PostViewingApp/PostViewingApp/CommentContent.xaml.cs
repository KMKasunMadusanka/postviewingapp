﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostViewingApp.Data;
using PostViewingApp.Net;
using Plugin.Messaging;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PostViewingApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommentContent : ContentPage
	{
        public ObservableCollection<Comment> PostList { get; }
        DataRetriewer _dr;
        int userId;
        public CommentContent (Post postData)
		{
			InitializeComponent ();

            this.IsBusy = false;
            Title = "Comments";
            PostList = new ObservableCollection<Comment>();
            commentView.ItemsSource = PostList;

            _dr = new DataRetriewer();
            userId = postData.userId;
            LoadData(postData.id);
        }

        private void LoadData(int id)
        {
            this.IsBusy = true;
            PostList.Clear();

            List<Comment> comments = _dr.GetComments(id);
            foreach (Comment c in comments)
            {
                PostList.Add(c);
            }
            this.IsBusy = false;
        }

        private void commentView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (Comment)e.SelectedItem;

            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                emailMessenger.SendEmail(item.email, "Regarding Resent Comment", "Hi, This is regarding Post Viewing App");
                
                var email = new EmailMessageBuilder()
                  .To(item.email)       
                  .Subject("Regarding Resent Comment")
                  .Body("Hi, This is regarding Post Viewing App")
                  .Build();

                emailMessenger.SendEmail(email);
            }

        }

        private void iconexample_Activated(object sender, EventArgs e)
        {
            Navigation.PushAsync(new UserContent(userId));
        }
    }
}