﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostViewingApp.Data;
using PostViewingApp.Net;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PostViewingApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserContent : ContentPage
	{
        User user;
        DataRetriewer _dr;
        public UserContent (int id)
		{
			InitializeComponent ();

            this.IsBusy = false;
            Title = "Post";
            user = new User();

            _dr = new DataRetriewer();
            LoadData(id);
        }

        private void LoadData(int id)
        {
            this.IsBusy = true;
            user = _dr.getUsers(id);

            user_content_page.Title = user.id + "'s Profile";
            name_field.Text = user.name;
            userName_field.Text = user.username;
            email_field.Text = user.email;
            website_field.Text = user.website;
            telephone_field.Text = user.phone;

            this.IsBusy = false;
        }
    }
}