﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using PostViewingApp.Data;
using PostViewingApp.Net;

namespace PostViewingApp
{
    public partial class PostContent : ContentPage
    {
        public ObservableCollection<Post> PostList { get; }
        DataRetriewer _dr;


        public PostContent()
        {
            InitializeComponent();

            this.IsBusy = false;
            Title = "Posts";
            PostList = new ObservableCollection<Post>();
            postListView.ItemsSource = PostList;

            _dr = new DataRetriewer();
            LoadData();
        }

       

        private void postListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var item = (Post)e.SelectedItem;
                Navigation.PushAsync(new CommentContent(item));
            }

            //Clear selection
           postListView.SelectedItem = null;
        }


        private void LoadData()
        {
            this.IsBusy = true;
            PostList.Clear();

            List<Post> posts = _dr.GetPost();
            foreach (Post p in posts)
            {
                PostList.Add(p);
            }
            this.IsBusy = false;
        }

       
    }
}
