﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PostViewingApp.Data;

namespace PostViewingApp.Net
{
    public class DataRetriewer
    {

        public List<Post> GetPost()
        {
            List<Post> postList = new List<Post>();

            using (WebClient wc = new WebClient())
            {
                string json = wc.DownloadString("https://jsonplaceholder.typicode.com/posts");

                if (!string.IsNullOrEmpty(json))
                {
                    postList = JsonConvert.DeserializeObject<Post[]>(json).ToList();
                }
            }

            return postList;
        }

        public List<Comment> GetComments(int id)
        {
            List<Comment> commentList = new List<Comment>();

            using (WebClient wc = new WebClient())
            {
                string json = wc.DownloadString("https://jsonplaceholder.typicode.com/posts/"+id+"/comments");

                if (!string.IsNullOrEmpty(json))
                {
                    commentList = JsonConvert.DeserializeObject<Comment[]>(json).ToList();
                }
            }

            return commentList;
        }

        public User getUsers(int id)
        {
            User user = new User();
            using (WebClient wc = new WebClient())
            {
                string json = wc.DownloadString("https://jsonplaceholder.typicode.com/users/"+id);

                if (!string.IsNullOrEmpty(json))
                {
                    user = JsonConvert.DeserializeObject<User>(json);
                }

                return user;
            }

        }

    }

}
